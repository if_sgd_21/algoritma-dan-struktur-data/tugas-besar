import java.util.Scanner;
//color on cmd
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;

public class Obat {
    void garis() {
        PrintStream out = null;
        try {
            out = new PrintStream(System.out, true, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        System.out.println("\033[0m=========================================");
    }
    void menu() {
        PrintStream out = null;
        try {
            out = new PrintStream(System.out, true, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        System.out.println("""        
    \033[0m=========================================
    \033[32m              Menu Data Obat             
    \033[0m=========================================
    \033[32m1. Masukkan ke Tumpukan                  
    \033[32m2. Keluarkan dari Tumpukan                         
    \033[32m3. Lihat Tumpukan                  
    \033[32m4. Keluar                        
    \033[0m=========================================
            """);
        System.out.print("\033[32mPilih Menu : ");
    }
    int kode;
    String nama,exp;
    Obat next;
    public static Scanner in=new Scanner(System.in);
    public static Scanner str=new Scanner(System.in);
    public void input(){
        System.out.print("Masukkan kode Obat    : ");
        kode=in.nextInt();
        System.out.print("Masukkan Nama Obat    : ");
        nama=str.nextLine();
        System.out.print("Masukkan EXP Obat     : ");
        exp=str.nextLine();
        next=null;
    }
    public void view(){
        System.out.println("Kode Obat       : "+kode);
        System.out.println("Nama Obat       : "+nama);
        System.out.println("EXP Obat        : "+exp);
    }
    public static void main(String[] args) {
        Obat ob = new Obat();
        int menu=0;
        linked2 st=new linked2();
        while(menu!=4){
            ob.menu();
            menu=in.nextInt();
            if(menu==1){
                Obat baru=new Obat();
                baru.input();
                st.push(baru);
            }
            else if(menu==2) st.pop();
            else if(menu==3) st.view();
            else if(menu==4) {
                System.out.println();
                ob.garis();
                System.out.println("Anda akan diarahkan ke Menu Utama");
                ob.garis();
                System.out.println();
                TugasBesar.main(args);
            }
            else {
                ob.garis();
                System.out.println("Menu tidak tersedia");
                ob.garis();
            }
        }
    }    
}
class linked2 {
    Obat top;
    public linked2(){
        top=null;
    }
    public void push(Obat a){
        if(top==null) top=a;
        else{
            a.next=top;
            top=a;
        }
    }
    public void pop(){
        if(top==null) System.out.println("KOSONG");
        else{
            System.out.println("Mengeluarkan . . .");
            top.view();
            top=top.next;
        }
    }
    public void view(){
        if(top==null) System.out.println("KOSONG");
        else{
            Obat ptr=top;
            while(ptr!=null){
                System.out.println("- - - - -");
                ptr.view();
                ptr=ptr.next;
            }
        }
    }
}
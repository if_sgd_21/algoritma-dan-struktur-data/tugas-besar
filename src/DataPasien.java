import java.util.Scanner;
//color on cmd
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;


public class DataPasien {
    void garis() {
        PrintStream out = null;
        try {
            out = new PrintStream(System.out, true, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        System.out.println("\033[0m=========================================");
    }
    void menu() {
        PrintStream out = null;
        try {
            out = new PrintStream(System.out, true, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        System.out.println("""        
    \033[0m=========================================
    \033[36m            Menu Data Pasien             
    \033[0m=========================================
    \033[36m1. Tambah Antrian Pasien                  
    \033[36m2. Pemanggilan Pasien                         
    \033[36m3. Tampilkan Antrian Pasien                  
    \033[36m4. keluar                        
    \033[0m=========================================
            """);
        System.out.print("\033[36mPilih Menu : ");
    }
    static int nomor;
    static String nama;
    static String Keperluan;
    DataPasien next;
    
    static Scanner in = new Scanner(System.in);
    static Scanner str = new Scanner(System.in);
    public void input(){
        System.out.println();
        System.out.print("=========================================\n");
        System.out.print("Nomor Antrian     : ");
        nomor = in.nextInt();
        System.out.print("Nama pasien       : ");
        nama = str.nextLine();
        System.out.print("Keperluan         : ");
        Keperluan = str.nextLine();
        System.out.println("=========================================");
        System.out.println();
        next=null;
    }
    public void read(){
        System.out.println();
        System.out.println("=========================================");
        System.out.println("Nomor Antrian   : "+ nomor);
        System.out.println("Nama pasien     : "+ nama);
        System.out.println("Keperluan       : "+ Keperluan);
        System.out.println("=========================================");
        System.out.println();
    }
    public static void main(String[] args){
        DataPasien dp = new DataPasien();
        int menu=0;
        linked que=new linked();
        while(menu!=4){
            dp.menu();
            menu=in.nextInt();

            if(menu==1)
            {
                que.enque();
            }
            else if (menu==2){
                que.deque();
            }
            else if(menu==3) {
                que.view();
            }
            else if(menu==4) {
                System.out.println();
                dp.garis();
                System.out.println("Anda akan diarahkan ke Menu Utama");
                dp.garis();
                System.out.println();
                TugasBesar.main(args);
            }
            else {
                dp.garis();
                System.out.println("Menu tidak tersedia");
                dp.garis();
            }
        }
    }
}
class linked{
    DataPasien head,tail;
    public linked(){
        head=null;
        tail=null;
    }
    public void enque(){
        DataPasien baru=new DataPasien();
        baru.input();
        if(head==null)head=baru;
        else tail.next=baru;
        tail=baru;
    }
    public void deque(){
        if(head==null) {
            System.out.println();
            System.out.println("=========================================");
            System.out.println("- Kosong -");
            System.out.println("=========================================");
        } else {
            System.out.println();
            System.out.println("=========================================");
            System.out.println("Masuk ke Ruangan Untuk Antrian Nomor : "+head.nomor);
            System.out.println("=========================================");
            System.out.println();
            head=head.next;
        }
    }
    public void view(){
        DataPasien temp=head;
        if(head==null) {
            System.out.println();
            System.out.println("=========================================");
            System.out.println("- Kosong -");
            System.out.println("=========================================");
        } else {
            while(temp!=null){
                temp.read();
                temp=temp.next;
            }
        }
    }
}

import java.util.Hashtable;
import java.util.Enumeration;
import java.util.Scanner;
//color on cmd
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
public class RuangRS {
    void menu() {
        PrintStream out = null;
        try {
            out = new PrintStream(System.out, true, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        System.out.println("""
    \033[0m=========================================
    \033[34m            Menu Informasi Ruangan
    \033[0m=========================================
    \033[34m1. Tampilkan Informasi Ruangan 
    \033[34m2. Cari Ruangan
    \033[34m3. Keluar
    \033[0m=========================================
            """);
    //set default color for entire program
        System.out.print("\033[34mPilih Menu : ");
    }
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        Scanner str = new Scanner(System.in);
        Enumeration names;
        String key;
        // creating hastable
        Hashtable<String, String> hashtable = new Hashtable<String, String>();
        hashtable.put("Ruang S",
                "ruang kamar jenazah");
        hashtable.put("Ruang R",
                "ruangan istirahat dokter ");
        hashtable.put("Ruang Q",
                "ruang jaga perawat ");
        hashtable.put("Ruang P",
                "ruangan apotek atau tempat pengambilan obat");
        hashtable.put("Ruang O",
                "ruangan laboratorium");
        hashtable.put("Ruang N",
                "ruang operasi");
        hashtable.put("Ruang M",
                "ruang radiologi");
        hashtable.put("Ruang L",
                "ruangan Klinik telinga hidung tenggorokan ");
        hashtable.put("Ruang K",
                "ruangan Klinik gigi");
        hashtable.put("Ruang J",
                "ruang perawatan untuk penderita kelainan tulang ");
        hashtable.put("Ruang I",
                "ruang perawatan penyakit jantung");
        hashtable.put("Ruang H",
                "ruang bersalin");
        hashtable.put("Ruang G",
                "ruang perawatan orang lanjut usia ");
        hashtable.put("Ruang F",
                "ruang perawatan bayi yang mengalami masalah kesehatan ");
        hashtable.put("Ruang E",
                "ruangan klinik anak");
        hashtable.put("Ruang D",
                "ruang perawatan intensif (ICU)");
        hashtable.put("Ruang C",
                "ruang unit gawat darurat (UGD)");
        hashtable.put("Ruang B",
                "ruang perawatan umum");
        hashtable.put("Ruang A",
                "ruangan tempat pendaftaran pasien ");
        RuangRS ruang = new RuangRS();
        ruang.menu();
        int pilihan = input.nextInt();
        while(pilihan != 3) {
            switch (pilihan) {
                case 1:
                    System.out.println(" Daftar ruangan : ");
                    Enumeration enu = hashtable.keys();
                    while (enu.hasMoreElements()) {
                        System.out.println(" " + enu.nextElement());
                    }
                    break;
                case 2:
                    System.out.print("\nInputkan Informasi ruangan yang akan dicari \t: ");
                    String cari = str.nextLine();
                    System.out.println("----------------------------------------------------------------");
                    System.out.println(" |" + cari + "| adalah " + hashtable.get(cari));
                    System.out.println("----------------------------------------------------------------");        
                    break;
                default:
                    System.out.println("Pilihan tidak tersedia");
                    break;
            }
            ruang.menu();
            pilihan = input.nextInt();
        }
        System.out.println("Anda akan diarahkan ke menu utama");
        TugasBesar.main(args);
    }
}